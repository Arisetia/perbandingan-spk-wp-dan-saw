
	<?php 
	$title = "Login";
	include('include/head.php'); 
	$err = "";
	if(isset($_POST['login'])){
		$user = $_POST['user'];
		$pass = MD5($_POST['pass']);
		
		$query = mysql_query("select * from akses_login where user_akses = '$user'");
		$result = mysql_fetch_assoc($query);
		
		if(mysql_num_rows($query) > 0){
			if($result['pass_akses'] == $pass){
				$_SESSION['login'] = $result['id_akses'];
				$_SESSION['role'] = WhatsRole($result['role']);
				header('location:index.php');
			}else{
				$err = "Kata sandi salah!";
			}
		}else{
			$err = "Username tidak terdaftar!";
		}
	}
	
	?>

<body>
	<div class="login-wrap customscroll d-flex align-items-center flex-wrap justify-content-center pd-20">
		<div class="login-box bg-white box-shadow pd-30 border-radius-5">
			<img src="assets/vendors/images/logo_black.png" alt="login" class="login-img">
			<h4 class="text-center mb-30">Login</h4>
			
			<form method="post" action="">
				<div class="input-group custom input-group-lg">
					<input type="text" class="form-control" name="user" id="user" placeholder="Username" required>
					<div class="input-group-append custom">
						<span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
					</div>
				</div>
				<div class="input-group custom input-group-lg">
					<input type="password" class="form-control" id="name" name="pass" placeholder="**********" required>
					<div class="input-group-append custom">
						<span class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i></span>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-group">
							<input class="btn btn-outline-primary btn-lg btn-block" name="login" type="submit" value="Masuk">
						</div>
					</div>
					
				</div>
				<small style="color:red"><?=$err;?></small>
			</form>
		</div>
		<center><small>&copy; Copyright <?=WEBNAME;?> <?=date('Y');?></small></center>
		<button class="btn btn-info btn-sm" id="admin">Admin</button> akses=> admin;admin
		<hr>
		<button class="btn btn-info btn-sm" id="user2">User</button> akses=> ridho;ridho
		
	</div>
	
	<?php include('include/script.php'); ?>
	<script>
		$(function(){
			$('#admin').click(function(){
				$("#user").val('admin');
				$("#name").val('admin');
			});
			$('#user2').click(function(){
				$("#user").val('ridho');
				$("#name").val('ridho');
			});
		});
	</script>
