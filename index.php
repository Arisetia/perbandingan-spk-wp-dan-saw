<?php
session_start();

if(!$_SESSION['login']){
	header('location:login.php');
}

$title = "Dashboard";
require "include/header.php";
?>

	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				<div class="row">
					<!--div class="col-md-6">
						<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="carousel-item active">
									<img class="d-block w-100 mCS_img_loaded" src="assets/vendors/images/banner/1.jpg" alt="First slide">
								</div>
								<div class="carousel-item">
									<img class="d-block w-100 mCS_img_loaded" src="assets/vendors/images/banner/2.jpg" alt="Second slide">
								</div>
								<div class="carousel-item">
									<img class="d-block w-100 mCS_img_loaded" src="assets/vendors/images/banner/3.jpg" alt="Third slide">
								</div>
								<div class="carousel-item">
									<img class="d-block w-100 mCS_img_loaded" src="assets/vendors/images/banner/4.jpg" alt="Third slide">
								</div>
							</div>
							<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div-->
					<div class="col-md-12">
						<div class="html-editor pd-20 bg-white border-radius-4 box-shadow mb-30">
							<br>
							<center><img src="assets/vendors/images/store.png"></center>
							<br>
							<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PT Kaurama Buana Antara adalah sebuah perusahaan media yang berdiri dan berkedudukan di Tangerang Selatan pada tanggal 19 Februari 2014. Kaurama berkehendak untuk memajukan Nusantara melalui karya-karya cipta yang dibutuhkan oleh masyarakat Nusantara, seturut dengan visi besarnya: Membabar jati diri Nusantara melalui karya cipta.</p>

							<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sebagai perusahaan media, Kaurama, pada permulaan, menitikberatkan usahanya di bidang penerbitan buku dan bentuk-bentuk media cetak lainnya, baik atas inisiatif sendiri atau bekerja sama dengan pihak-pihak lain, juga penyediaan jasa konsultasi media bagi pihak-pihak yang membutuhkan. Kaurama memiliki misi untuk menjadi perusahaan terbaik dalam menjalankan bidang usaha penerbitan, percetakan, konsultasi media cetak dan elektronik, dan perdagangan umum.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include('include/script.php'); ?>