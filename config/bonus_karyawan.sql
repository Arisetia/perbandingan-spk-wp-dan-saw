-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2018 at 10:25 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bonus_karyawan`
--

-- --------------------------------------------------------

--
-- Table structure for table `akses_login`
--

CREATE TABLE IF NOT EXISTS `akses_login` (
`id_akses` int(5) NOT NULL,
  `nama_akses` varchar(40) NOT NULL,
  `user_akses` varchar(15) NOT NULL,
  `pass_akses` varchar(32) NOT NULL,
  `role` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `akses_login`
--

INSERT INTO `akses_login` (`id_akses`, `nama_akses`, `user_akses`, `pass_akses`, `role`) VALUES
(1, 'Ari', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
(8, 'Ari Setiawan', 'ridho', '926a161c6419512d711089538c80ac70', 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_kriteria`
--

CREATE TABLE IF NOT EXISTS `detail_kriteria` (
`id_dk` int(10) NOT NULL,
  `nama_dk` varchar(40) NOT NULL,
  `bobot_config` text,
  `type_dk` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `detail_kriteria`
--

INSERT INTO `detail_kriteria` (`id_dk`, `nama_dk`, `bobot_config`, `type_dk`) VALUES
(1, 'Bli', '{"bobot_final":"4","1":{"range":"1-5","ketetapan":"kurang","bobot":"1"},"2":{"range":"6-9","ketetapan":"cukup","bobot":"2"},"3":{"range":"10-11","ketetapan":"baik","bobot":"3"}}', 1),
(2, 'pajak', '{"bobot_final":"4","1":{"range":"1-4","ketetapan":"kurang","bobot":"1"},"2":{"range":"5-7","ketetapan":"cukup","bobot":"2"},"3":{"range":"8-10","ketetapan":"baik","bobot":"3"}}', 1),
(3, 'kancut', '{"bobot_final":"8","1":{"range":"1-10","ketetapan":"kurang","bobot":"1"},"2":{"range":"11-20","ketetapan":"cukup","bobot":"2"},"3":{"range":"21-30","ketetapan":"baik","bobot":"3"}}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `id_karyawan` varchar(8) NOT NULL,
  `nama_karyawan` varchar(40) NOT NULL,
  `dob` date NOT NULL,
  `jenis_kelamin` char(3) NOT NULL,
  `domisili` varchar(100) NOT NULL,
  `mulai_kontrak` date NOT NULL,
  `selesai_kontrak` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `nama_karyawan`, `dob`, `jenis_kelamin`, `domisili`, `mulai_kontrak`, `selesai_kontrak`) VALUES
('KBA00001', 'Ajis', '2018-09-07', 'L', '', '2018-09-06', '2018-09-15'),
('KBA00002', 'Babe', '2018-09-06', 'L', '', '2018-09-02', '2018-09-08');

-- --------------------------------------------------------

--
-- Table structure for table `klasifikasi`
--

CREATE TABLE IF NOT EXISTS `klasifikasi` (
  `id_dk` int(10) NOT NULL,
  `id_karyawan` varchar(8) NOT NULL,
  `nilai` double NOT NULL,
`id_klasifikasi` int(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `klasifikasi`
--

INSERT INTO `klasifikasi` (`id_dk`, `id_karyawan`, `nilai`, `id_klasifikasi`) VALUES
(1, 'KBA00001', 5, 1),
(2, 'KBA00001', 7, 2),
(1, 'KBA00002', 8, 3),
(2, 'KBA00002', 7, 5),
(3, 'KBA00001', 10, 6),
(3, 'KBA00002', 30, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses_login`
--
ALTER TABLE `akses_login`
 ADD PRIMARY KEY (`id_akses`);

--
-- Indexes for table `detail_kriteria`
--
ALTER TABLE `detail_kriteria`
 ADD PRIMARY KEY (`id_dk`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
 ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `klasifikasi`
--
ALTER TABLE `klasifikasi`
 ADD PRIMARY KEY (`id_klasifikasi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akses_login`
--
ALTER TABLE `akses_login`
MODIFY `id_akses` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `detail_kriteria`
--
ALTER TABLE `detail_kriteria`
MODIFY `id_dk` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `klasifikasi`
--
ALTER TABLE `klasifikasi`
MODIFY `id_klasifikasi` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
