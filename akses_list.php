
	<?php
		$title = "List Akses Login";
		include('include/header.php'); 
		if(!$_SESSION['login']){
			header('location:login.php');
		}
		if($_SESSION['role'] == WhatsRole(0)){
			header('location:403.php');
		}
		$errInfo = "";
		if(isset($_GET['del'])){
			$id = $_GET['del'];
			if($id != 1){
				$query = mysql_query("delete from akses_login where id_akses = '$id'");
				if($query){
					header("location:akses_list.php");
				}
			}else{
				$errInfo = "<div class=\"alert alert-danger\" role=\"alert\">
						Maaf, Master Admin utama tidak dapat dihapus !
					</div>";
			}
			
		}
		if(isset($_POST['update'])){
			if($_POST['role'] == 'on'){
				$role = 0;
			}else{
				$role = 1;
			}
			$id=$_POST['update'];
			$nama = $_POST['nama'];
			$user = $_POST['user'];
			if(!empty($_POST['pass'])){
				$pass = MD5($_POST['pass']);
				$query = mysql_query("update akses_login set user_akses= '$user', nama_akses='$nama', pass_akses='$pass', role = '$role' where id_akses = '$id'");
			}else{
				$query = mysql_query("update akses_login set user_akses= '$user', nama_akses='$nama', role = '$role' where id_akses = '$id'");
			}
			if($query){
				header("location:akses_list.php");
			}
		}
		if(isset($_POST['insert'])){
			if($_POST['role'] == 'on'){
				$role = 0;
			}else{
				$role = 1;
			}
			$nama = $_POST['nama'];
			$user = $_POST['user'];
			$pass = MD5($_POST['pass']);
			$queryAdd = mysql_query("insert into akses_login (user_akses, nama_akses, pass_akses, role) value ('$user','$nama','$pass','$role')");
			if($queryAdd){
				header("location:akses_list.php");
			}
		}
	?>
	<link rel="stylesheet" type="text/css" href="<?=URL;?>assets/js/switchery/dist/switchery.css">
	<link rel="stylesheet" type="text/css" href="<?=URL;?>assets/js/datatables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?=URL;?>assets/js/datatables/media/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="<?=URL;?>assets/js/datatables/media/css/responsive.dataTables.css">
	
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4><?=$title;?></h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="<?=URL;?>">Dashboard</a></li>
									<li class="breadcrumb-item active" aria-current="page"><?=$title;?></li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<div class="html-editor pd-20 bg-white border-radius-4 box-shadow mb-30">
					<?=$errInfo;?>
					<!-- Medium modal -->
					<div class="col-md-4 col-sm-12">
							
							<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#TambahAkses">Tambah Akses</button>
							
							<div class="modal fade" id="TambahAkses" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered">
									<div class="modal-content">
									<form method="post" action="">
										<div class="modal-header">
											<h4 class="modal-title" id="myLargeModalLabel">Tambah Akses Login</h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										</div>
										<div class="modal-body">
											
												<div class="form-group">
													<lable>Role</lable>
													<input type="checkbox" name="role" checked class="switch-btn" data-color="#0099ff"> 
													<span>[Admin/User]</span> 
												</div>
												<div class="form-group">
													<label>Nama Lengkap</label>
													<input class="form-control" type="text" name="nama" placeholder="Nama Lengkap" required>
												</div>
												<div class="form-group">
													<label>Username</label>
													<input class="form-control" name="user" type="text" placeholder="Username" required>
												</div>
												<div class="form-group">
													<label>Password</label>
													<input class="form-control" name="pass" type="password" placeholder="Password" required>
												</div>
											
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
											<button type="submit" class="btn btn-primary">Tambah</button>
											<input type="hidden" name="insert">
										</div>
										</form>
									</div>
								</div>
							</div>
					
					</div>
					<hr>
					<table class="data-table stripe hover nowrap">
							<thead>
								<tr>
									<th style="width:70px;" class="table-plus datatable-nosort">No.</th>
									<th>Nama</th>
									<th class="table-plus datatable-nosort">Username</th>
									<th class="table-plus datatable-nosort">Role</th>
									<th style="width:90px;" class="table-plus datatable-nosort">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
								$aksesQuery = mysql_query("select * from akses_login order by id_akses ASC");
								$akses = mysql_fetch_assoc($aksesQuery);
								$num = 0;
								do{
									$num++;
							?>
								<tr>
									<td><?=$num;?></td>
									<td><?=$akses['nama_akses'];?></td>
									<td><?=$akses['user_akses'];?></td>
									<td><?=WhatsRole($akses['role']);?></td>
									<td>
										<a class="btn btn-sm btn-success text-white" data-toggle="modal" data-target="#updateId<?=$akses['user_akses'];?>"><i class="fa fa-pencil"></i> Edit</a>
										<div class="modal fade" id="updateId<?=$akses['user_akses'];?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-dialog-centered">
												<div class="modal-content">
													<form method="post" action="">
														<div class="modal-header">
															<h4 class="modal-title">Ubah Akses Login</h4>
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
														</div>
														<div class="modal-body">	
															<div class="form-group">
																<lable>Role</lable>
																<input type="checkbox" name="role" <?php if($akses['role'] == 0){echo "checked";} ?> class="switch-btn" data-color="#0099ff"> 
																<span>[Admin/User]</span> 
															</div>
															<div class="form-group">
																<label>Nama Lengkap</label>
																<input class="form-control" value="<?=$akses['nama_akses'];?>" type="text" name="nama" placeholder="Nama Lengkap" required>
															</div>
															<div class="form-group">
																<label>Username</label>
																<input class="form-control" value="<?=$akses['user_akses'];?>" name="user" type="text" placeholder="Username" required>
															</div>
															<a data-toggle="collapse" data-target="#Show<?=$akses['id_akses'];?>" style="cursor:pointer">Ubah Password <i class="fa fa-angle-down" ></i></a>
															<div id="Show<?=$akses['id_akses'];?>" class="collapse">
															<div class="form-group" >
																<label>Password</label>
																<input class="form-control" name="pass" type="password" placeholder="Password">
															</div>	
															</div>	
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
															<button type="submit" class="btn btn-primary">Simpan</button>
															<input type="hidden" name="update" value="<?=$akses['id_akses'];?>">
														</div>
													</form>
													
												</div>
											</div>
										</div>
										<?php if($akses['id_akses'] != 1){ ?>
										<a class="btn btn-sm btn-danger text-white" onclick="confirmDelete(<?=$akses['id_akses'];?>);"><i class="fa fa-trash"></i> Delete</a>
										<?php } ?>	
									</td>
								</tr>
								
								<?php } while($akses = mysql_fetch_assoc($aksesQuery)); ?>
							</tbody>
						</table>
				</div>
			</div>
			
		</div>
	</div>
	
	<?php include('include/script.php'); ?>
	<script src="<?=URL;?>assets/js/switchery/dist/switchery.js"></script>
	<script src="<?=URL;?>assets/js/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?=URL;?>assets/js/datatables/media/js/dataTables.bootstrap4.js"></script>
	<script src="<?=URL;?>assets/js/datatables/media/js/dataTables.responsive.js"></script>
	<script src="<?=URL;?>assets/js/datatables/media/js/responsive.bootstrap4.js"></script>
	
	<script>
		$('document').ready(function(){
			var elems = Array.prototype.slice.call(document.querySelectorAll('.switch-btn'));
			$('.switch-btn').each(function() {
				new Switchery($(this)[0], $(this).data());
				//alert($(this)[0]);
			});
			$('.data-table').DataTable({
				scrollCollapse: true,
				autoWidth: false,
				responsive: true,
				columnDefs: [{
					targets: "datatable-nosort",
					orderable: false,
				}],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"language": {
					"info": "_START_-_END_ dari _TOTAL_ data",
					searchPlaceholder: "Cari Akses"
				},
			});
			
		});
		function confirmDelete(id){
			var c = confirm("Yakin akan menghapus data?");
			if(c== true){
				window.location.href="<?=URL;?>akses_list.php?del="+id;
			}
		}
		
	</script>
	</body>
</html>