<?php
$title = "Detail Kriteria";
require"include/header.php";

$data_master_bobot = array("bobot_final" => "5");
//$data_master_bobot['bobot'] = $dataBobot;
if(!$_SESSION['login']){
	header('location:login.php');
}
if($_SESSION['role'] == WhatsRole(1)){
	header('location:403.php');
}
if(isset($_GET['getdelete'])){
	$id = $_GET['getdelete'];
	$delKlasifikasi = mysql_query("delete from klasifikasi where id_dk = '$id'");
	if($delKlasifikasi){
		$delKriteria = mysql_query("delete from detail_kriteria where id_dk = '$id'");
		if($delKriteria){
			header('location:detail_kriteria.php');
		}
	}
	
}
if(isset($_POST['insert'])){

	##INISIALISASI
	$name 	= $_POST['nama'];
	$tipe	= $_POST['tipe'];
	$data_master_bobot['ketetapan'] = $_POST['tetap'];
	$dmb = json_encode($data_master_bobot);
	
	$query = mysql_query("insert into detail_kriteria (nama_dk, bobot_config, type_dk) value ('$name','$dmb','$tipe')");
	if($query){
		header("location:detail_kriteria.php");
	}
	
}
if(isset($_POST['update'])){
	##INISIALISASI
	$name 	= $_POST['nama'];
	$tipe		= $_POST['tipe'];
	$id		= $_POST['update'];
	$data_master_bobot['ketetapan'] = $_POST['tetap'];
	$dmb = json_encode($data_master_bobot);
	
	$query = mysql_query("update detail_kriteria set nama_dk = '$name', type_dk = '$tipe', bobot_config = '$dmb' where id_dk = '$id'");
	if($query){
		header("location:detail_kriteria.php");
	}
	
}

$query 	= mysql_query("select * from detail_kriteria order by id_dk DESC");
$data 	= mysql_fetch_assoc($query);
$row 	= mysql_num_rows($query);
?>
	<link rel="stylesheet" type="text/css" href="<?=URL;?>assets/js/datatables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?=URL;?>assets/js/datatables/media/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="<?=URL;?>assets/js/datatables/media/css/responsive.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?=URL;?>assets/js/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css">
	
		<div class="main-container">
			<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
				<div class="min-height-200px">
					<div class="page-header">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div class="title">
									<h4><?=$title;?></h4>
								</div>
								<nav aria-label="breadcrumb" role="navigation">
									<ol class="breadcrumb">
										<li class="breadcrumb-item"><a href="<?=URL;?>">Dashboard</a></li>
										<li class="breadcrumb-item active" aria-current="page"><?=$title;?></li>
									</ol>
								</nav>
							</div>
						</div>
					</div>
					<div class="html-editor pd-20 bg-white border-radius-4 box-shadow mb-30">
						<a data-target="#AddKriteria" data-toggle="modal" class="text-white btn btn-info btn-sm">Tambah</a><hr>
						<table id="dataTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th width="60px" class="table-plus datatable-nosort">No</th>
									<th>Nama</th>
									<th>Type</th>
									<th>Bobot Ketetapan</th>
									<th width="180px" class="table-plus datatable-nosort">Aksi</th>
								</tr>
							</thead>
							<tbody>
							
							<?php 
							if($row > 0){
							$num = 0; do{ $num++; 
							$Json = $data['bobot_config'];
							$resJson = objectToArray(json_decode($Json));
							?>
								<tr>
									<td><?=$num;?></td>
									<td><?=$data['nama_dk'];?></td>
									<td><?=convType($data['type_dk']);?></td>
									<td><?=$resJson['ketetapan'];?></td>
									<td>
										<a class="btn btn-sm btn-success text-white" data-toggle="modal" data-target="#EditKriteria<?=$data['id_dk'];?>">Edit</a>
										<a class="btn btn-sm btn-danger text-white" data-toggle="modal" data-target="#HapusKriteria<?=$data['id_dk'];?>" >Hapus</a>
										<!-- Hapus Kriteria -->
											<div class="modal fade" id="HapusKriteria<?=$data['id_dk'];?>" tabindex="-1" role="dialog" aria-hidden="true">
												<div class="modal-dialog modal-dialog-centered" role="document">
													<div class="modal-content">
														<div class="modal-body text-center font-18">
															<h4 class="padding-top-30 mb-30 weight-500">Yakin akan menghapus data Kriteria ini ?</h4>
															<div class="padding-bottom-30 row" style="max-width: 170px; margin: 0 auto;">
																<div class="col-6">
																	<button type="button" class="btn btn-secondary border-radius-100 btn-block confirmation-btn" data-dismiss="modal"><i class="fa fa-times"></i></button>
																	Tidak
																</div>
																<div class="col-6">
																	<button class="btn btn-primary border-radius-100 btn-block confirmation-btn hapusData" data="<?=$data['id_dk'];?>" ><i class="fa fa-check"></i></button>
																	Ya
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										
										<!-- EDIT KRITERIA BARU -->
										<div class="modal fade bs-example-modal-lg" id="EditKriteria<?=$data['id_dk'];?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg modal-dialog-centered">
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title" id="myLargeModalLabel">Edit Data Kriteria</h4>
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													</div>
													<form method="post" action="">
														<div class="modal-body">		
															<div class="row">
																<div class="col-md-5">
																	<div class="form-group">
																		<lable>Nama Kriteria<sup>*</sup></lable>
																		<input class="form-control" type="text" name="nama" placeholder="Nama kriteria" value="<?=$data['nama_dk'];?>">
																	</div>
																</div>
																<div class="col-md-4">
																	<div class="form-group">
																		<lable>Tipe Kriteria<sup>*</sup></lable>
																		<select name="tipe" class="form-control vn2" required>
																			<option value="1" <?php if($data['type_dk'] == 1){echo"selected";}?>>benefit</option>
																			<option value="0" <?php if($data['type_dk'] == 0){echo"selected";}?>>cost</option>
																		</select>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="form-group">
																		<lable>Bobot Ketetapan<sup>*</sup></lable>
																		<select name="tetap" class="form-control vx2" required>
																			
																			<option value="1" <?php if($resJson['ketetapan'] == 1){echo"selected";}?>>1</option>
																			<option value="2" <?php if($resJson['ketetapan'] == 2){echo"selected";}?>>2</option>
																			<option value="3" <?php if($resJson['ketetapan'] == 3){echo"selected";}?>>3</option>
																			<option value="4" <?php if($resJson['ketetapan'] == 4){echo"selected";}?>>4</option>
																			<option value="5" <?php if($resJson['ketetapan'] == 5){echo"selected";}?>>5</option>
																			
																		</select>
																	</div>
																</div>
															</div>
															
														</div>
														<div class="modal-footer">
														<input type="hidden" name="update" value="<?=$data['id_dk'];?>">
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
															<button type="submit" class="btn btn-primary">Simpan</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</td>
								</tr>
							<?php }while($data 	= mysql_fetch_assoc($query)); }else{?>
							
							<tr><td colspan="4"><center>Belum ada data ditampilkan !</center></td></tr>
							<?php
							}?>
							</tbody>

						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- TAMBAH KRITERIA BARU -->
		<div class="modal fade bs-example-modal-lg" id="AddKriteria" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myLargeModalLabel">Tambah Kriteria Baru</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<form method="post" action="">
						<div class="modal-body">
							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<lable>Nama Kriteria<sup>*</sup></lable>
										<input class="form-control" type="text" name="nama" placeholder="Nama kriteria">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<lable>Tipe Kriteria<sup>*</sup></lable>
										<select name="tipe" class="form-control vn" required>
											<option value="">-Pilih tipe Kriteria</option>
											<option value="1">benefit</option>
											<option value="0">cost</option>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<lable>Bobot Ketetapan<sup>*</sup></lable>
										<select name="tetap" class="form-control vx" required>
											
											
										</select>
									</div>
								</div>
							</div>
							
						</div>
						<div class="modal-footer">
						<input type="hidden" name="insert" value="Simpan">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
			</div>
		</div>
<?php require"include/script.php";?>
		<script src="<?=URL;?>assets/js/datatables/media/js/jquery.dataTables.min.js"></script>
		<script src="<?=URL;?>assets/js/datatables/media/js/dataTables.bootstrap4.js"></script>
		<script src="<?=URL;?>assets/js/datatables/media/js/dataTables.responsive.js"></script>
		<script src="<?=URL;?>assets/js/datatables/media/js/responsive.bootstrap4.js"></script>
		<script src="<?=URL;?>assets/js/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js"></script>
		
		<script>
		/* 
		var cost = '<option value="">-Ketetapan Cost</option><option value="1">Sangat Baik</option><option value="2">Baik</option><option value="3">Cukup</option><option value="4">Kurang</option><option value="5">Sangat Kurang</option>';
				var benefit = '<option value="">-Ketetapan Benefit</option><option value="1">Sangat Kurang</option><option value="2">Kurang</option><option value="3">Cukup</option><option value="4">Baik</option><option value="5">Sangat Baik</option>';
				$(".vn").change(function(){
					//alert(1);
					var c = $(".vn").val();
					$(".vx").html("");
					if(c == true){
						$(".vx").append(benefit);
					}else{
						$(".vx").append(cost);
					}
				}); */
				$(".vx").append('<option value="">-Ketetapan Cost</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option>');
			$('document').ready(function(){
				$('#dataTable').DataTable({
					"language": {
						"info": "_START_-_END_ dari _TOTAL_ data",
						searchPlaceholder: "Cari Kriteria"
					}
				});
				$(".hapusData").click(function(){
					var id = $(this).attr('data');
					window.location.href="detail_kriteria.php?getdelete="+id;
					//alert(id);
				});
				
				
				
			});
		</script>
	</body>
</html>