	<?php
		@session_start();
		require "config/define.php";
		require "config/database.php";
		require "config/function.inc.php";
		
	?>
<!DOCTYPE html>
<html>
	<head>
	
	<meta charset="utf-8">
	<title><?=$title." - ".WEBNAME;?></title>

	
	<link rel="shortcut icon" href="assets/vendors/images/favicon.png">
	<link rel="icon" type="image/png" href="assets/vendors/images/favicon.png" sizes="32x32">
	
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link rel="stylesheet" href="assets/vendors/styles/style.css">
	
	<style>
		.mt50{
			margin-top: 50px;
		}
		kbainfo{
			position: absolute;
			color: #fff;
			background-color: red;
			padding: 0 5px;
			border-radius: 50px;
			font-size: 12px;
			margin-top: -6px;
			margin-left: -10px;
		}
		.mth{text-align:center}
	</style>
	</head>