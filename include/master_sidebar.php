	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="index.php">
				<img src="assets/vendors/images/logo.png" alt="">
			</a>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li class="dropdown">
						<a href="<?=URL;?>" class="dropdown-toggle no-arrow">
							<span class="fa fa-home"></span><span class="mtext">Dashboard</span>
						</a>
					</li>
					
					<li>
						<a href="report.php" class="dropdown-toggle no-arrow">
							<span class="fa fa-file"></span><span class="mtext">Laporan</span>
						</a>
					</li>
					<li class="dropdown">
						<a href="akses_list.php" class="dropdown-toggle no-arrow">
							<span class="fa fa-key"></span><span class="mtext">Akses Login</span>
						</a>
						
					</li>
					<li>
						<a href="logout.php" class="dropdown-toggle no-arrow">
							<span class="fa fa-sign-out"></span><span class="mtext">Logout</span>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>