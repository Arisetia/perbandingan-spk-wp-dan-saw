	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="<?=URL;?>">
				<img src="<?=URL;?>assets/vendors/images/logo.png" alt="">
			</a>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li class="dropdown">
						<a href="<?=URL;?>" class="dropdown-toggle no-arrow">
							<span class="fa fa-home"></span><span class="mtext">Dashboard</span>
						</a>
					</li>
					<li>
						<a href="karyawan.php" class="dropdown-toggle no-arrow">
							<span class="fa fa-group"></span><span class="mtext">Data Karyawan</span>
						</a>
					</li>
					<li>
						<a href="detail_kriteria.php" class="dropdown-toggle no-arrow">
							<span class="fa fa-retweet"></span><span class="mtext">Detail Kriteria</span>
						</a>
					</li>
					<li>
						<a href="report.php" class="dropdown-toggle no-arrow">
							<span class="fa fa-file"></span><span class="mtext">Laporan</span>
						</a>
					</li>
					<li>
						<a href="logout.php" class="dropdown-toggle no-arrow">
							<span class="fa fa-sign-out"></span><span class="mtext">Logout</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>