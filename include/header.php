	<?php include "head.php"; ?>
	<div class="pre-loader"></div>
	<div class="header clearfix">
		<div class="header-right">
			<div class="brand-logo">
				<a href="index.php">
					<img src="assets/vendors/images/logo.png" alt="" class="mobile-logo">
				</a>
			</div>
			<div class="menu-icon">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon"><i class="fa fa-user-o"></i></span>
						<span class="user-name">Selamat Datang, <?=MyName($_SESSION['login']);?></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						
						<a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
	
	if($_SESSION['role'] == WhatsRole(1)){
		require "include/master_sidebar.php";
	}else{
		require "include/sidebar.php";
	}
	
	
	?>